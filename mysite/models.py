from django.db import models

class Espejo(models.Model):
    alto = models.FloatField()
    ancho = models.FloatField()
    color = models.BooleanField(default=False)
    pedestal = models.BooleanField(default=False)

    def __str__(self):
        return f"Espejo ({self.alto}x{self.ancho}cm)"


class Pedido(models.Model):
    espejos = models.ManyToManyField(Espejo)
