from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path('eliminar_pedidos/', views.eliminar_pedidos, name='eliminar_pedidos'),
    path('calcular/', views.calcular, name='calcular'),
    path('contacto/', views.contacto, name='contacto'),
    path('productos/', views.productos, name='productos'),
    path('nosotros/', views.nosotros, name='nosotros'),
]
