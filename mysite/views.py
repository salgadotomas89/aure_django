from django.shortcuts import render
from django.http import HttpResponse
import logging

logger = logging.getLogger(__name__)

def eliminar_pedidos():
    return HttpResponse("Datos de Pedidos eliminados.") 

def home(request):
    return render(request, 'home.html')

def productos(request):
    return render(request, 'productos.html')

def calcular(request):
    return render(request, 'calcular.html')

def contacto(request):
    
    return render(request, 'contacto.html')

def nosotros(request):
    return render(request, 'nosotros.html')




